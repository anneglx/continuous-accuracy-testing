import numpy as np
import pandas as pd

'''
testing utils
import sys
sys.path.append('../')'''

from geo_info.geo_info import GeoInfo
from results_utils import melt_result


# Equals to ``km2r(100)`` (See algo-manager.defaults)
MAX_STATION_DISTANCE = 0.015678558132172078


def arrange_stations_input(raw_data, get_units=True):
    """
    Process stations-raw-data dictionary and rearrange the data.
    Return a DataFrame with the data and a dictionary with pollutant units."""
    # Make DataFrame
    data = pd.DataFrame(raw_data)
    # Remove non-hourly pollutants
    mask = ~data['pollutant_name'].str.contains('hr')
    data = data[mask]
    # Get unique pollutants and units
    if get_units:
        pol_unit = _extract_units(data)
    else:
        pol_unit = None
    # Pivot

    index_columns = ['lat', 'lon']
    if 'country_code' in data.columns:
        index_columns.append('country_code')

    data_p = pd.pivot_table(data, values='value',
                            columns=['pollutant_name'],
                            index=index_columns).reset_index(drop=False)
    return data_p, pol_unit


def arrange_traffic_input(raw_data, get_units=True):
    """
    Process stations-raw-data dictionary and rearrange the data.
    Return a separate DataFrame for fixed-traffic and ambient data, and a
    dictionary with pollutant units.
    """
    # Make DataFrame
    data = pd.DataFrame(raw_data)
    # Remove non-hourly pollutants
    mask = ~data['pollutant_name'].str.contains('hr')
    data = data[mask]
    # Make sure we have traffic data
    if 'value_before_traffic_fixes' not in data.columns.tolist():
        return None, None, None
    # Get unique pollutants and units
    if get_units:
        pol_unit = _extract_units(data)
    else:
        pol_unit = None
    # Separate the traffic-fixed and ambient data
    mask = ~np.isnan(data['value_before_traffic_fixes'])
    fixed = data[mask].copy()
    ambient = data[~mask].copy()
    # Pivot
    fixed_p = pd.pivot_table(fixed, values='value',
                             columns=['pollutant_name'],
                             index=['lat', 'lon']).reset_index(drop=False)
    ambient_p = pd.pivot_table(ambient, values='value',
                               columns=['pollutant_name'],
                               index=['lat', 'lon']).reset_index(drop=False)
    return fixed_p, ambient_p, pol_unit


def _extract_units(data):
    pollutants = pd.unique(data['pollutant_name']).tolist()
    pol_unit = {}
    for pol in pollutants:
        u = pd.unique(data[data['pollutant_name'] == pol]['units']).tolist()
        if len(u) > 1:
            raise NotImplementedError('Cannot handle more than 1 unit '
                                      'per pollutant')
        pol_unit[pol] = u[0]
    return pol_unit


def add_col_latlon(data):
    """Add a column with a string of rounded lat-lon values"""
    lat = (data['lat'] * 10000).astype(int).astype(str)  # .round(decimals=0)
    lon = (data['lon'] * 10000).astype(int).astype(str)  # .round(decimals=0)
    data['lat_lon'] = lat.str.cat(lon, sep='_')
    return data


def groupby_latlon(data):
    """Use rounded coords as unique-id and group the data with average"""
    data_lat_lon = add_col_latlon(data.copy())
    grouped = data_lat_lon.groupby('lat_lon')

    if 'country_code' in data_lat_lon.columns:
        return pd.concat([grouped.agg({'country_code': 'first'}), grouped.mean()], axis=1)
    else:
        return grouped.mean()


def exclude_data_by_index(data, key_value, index_level_name='lat_lon'):
    """
    Separate the data by the ``key_value`` given. Return two DataFrames, one
    for each segment. This func assumes ``data`` has an index-level with a
    name matching ``index_level_name`` (e.g. 'lat_lon').
    """
    mask = ~(data.index.get_level_values(index_level_name) == key_value)
    data_all_but = data.loc[mask, :].copy()
    data_removed = data.loc[~mask, :].copy()
    return data_all_but, data_removed


def mask_stations_by_bounds(data, bounds):
    mask_lat = np.bitwise_and(data['lat'] >= bounds[0],
                              data['lat'] <= bounds[1])
    mask_lon = np.bitwise_and(data['lon'] >= bounds[2],
                              data['lon'] <= bounds[3])
    mask = np.bitwise_and(mask_lat, mask_lon)
    return data[mask]


def loo(tt, gt, interp_algo, index_level_name='lat_lon'):
    """
    Preform a leave-one-out test with the input algorithms and data.
    This func assumes ``tt`` and ``gt`` have an index-level with a name
    matching ``index_level_name`` (e.g. 'lat_lon').

    Parameters
    ----------
    tt : DataFrame
        Data to test
    gt : DataFrame
        Data to use as ground truth
    interp_algo : list of functions
        The algorithms to calculate
    index_level_name : string
        The name of the index-level to use for indexing with ``tt`` and ``gt``
    """
    res_all = []
    num_lines = gt.shape[0]
    for line in range(num_lines):
        key_value = gt.index.get_level_values(index_level_name)[line]

        # Get point data (removed station)
        point_data = pd.DataFrame(gt.loc[key_value, :].copy()).T
        point_data.index.set_names(gt.index.names, inplace=True)

        # Get all the rest of the data
        test_data, _ = exclude_data_by_index(tt, key_value, index_level_name)
        # Exclude stations that are too far away
        test_data = _filter_far_stations(test_data, point_data[['lat', 'lon']])
        # In case there are no close stations
        if len(test_data) == 0:
            continue

        res = station_spatial_interp(interp_algo, test_data, point_data)
        res = melt_result(res, point_data)

        if res is not None:
            res_all.append(res)

    return res_all


def _filter_far_stations(data, coords, degrees_lat=1.0):
    """
    Keep only lines with stations that fall inside a box around ``coords``.
    The box's y-magin (lat) is defined by ``degrees_lat``. The x-margin (lon)
    varies by 1 degree, according to the location on the y axis.
    Both ``data`` and ``coords`` should be DataFrames with ``lat`` and ``lon``
    columns.
    Returns a DataFrame.
    """
    data_lat = data['lat'].values
    data_lon = data['lon'].values
    point_lat = coords['lat'].values
    point_lon = coords['lon'].values

    if abs(point_lat) < 50.0:
        degrees_lon = degrees_lat
    else:
        degrees_lon = degrees_lat + 1.0
    lat_mask = np.bitwise_and(data_lat >= point_lat - degrees_lat,
                              data_lat <= point_lat + degrees_lat)
    lon_mask = np.bitwise_and(data_lon >= point_lon - degrees_lon,
                              data_lon <= point_lon + degrees_lon)
    return data[np.bitwise_and(lat_mask, lon_mask)]


def station_spatial_interp(algorithms, data, coords):
    """
    Calculate each algorithm result at the ``coords``, with the ``data`` given.
    Returns a DataFrame.
    """
    res_all = []
    coords_no_index = coords.reset_index(drop=True)
    data_no_index = data.reset_index(drop=True)
    for algo in algorithms:
        # The next line is equivalent to: IDW(grid, stations, max_distance)
        res = algo(coords_no_index, data_no_index,
                   max_distance=MAX_STATION_DISTANCE)

        # To the algorithm's result we add the df index,
        # which is the removed station's name/id
        res['lat_lon'] = coords.index.get_level_values('lat_lon')[0]
        # res['local_id'] = coords.index.get_level_values('local_id')[0]

        # Also add the algorithm's name
        res['algo_name'] = algo.__name__
        res_all.append(res)

    return pd.concat(res_all, axis=0, ignore_index=True)


def add_traffic_overlay(stations, traffic_overlay):
    """
    Add traffic overlay values to the stations measurements (after catchup
    and traffic fixes). To match between a station's location and the grid
    point it is at, we convert the station's coords to index (x, y), using
    the GeoInfo class.
    """
    # Add index (x, y) columns to stations data
    stations['x'] = None
    stations['y'] = None
    geo = GeoInfo((-180, -90, 180, 90))
    for i, si in enumerate(stations.itertuples()):
        stations.loc[i, 'y'], stations.loc[i, 'x'] = geo.geo_to_closest_index(si.lat, si.lon)
    stations.set_index(['y', 'x', 'pollutant_name'], inplace=True)
    # Rearrange the traffic data
    traffic_overlay = pd.DataFrame(traffic_overlay)
    # traffic_overlay.drop(columns=['fc'], inplace=True)          #todo remove useless fix
    traffic_pols = list(set(traffic_overlay.columns.tolist()) - {'x', 'y'})
    traffic_overlay['x'] = traffic_overlay['x'].astype(int)
    traffic_overlay['y'] = traffic_overlay['y'].astype(int)
    # traffic_overlay[traffic_pols] = traffic_overlay[traffic_pols].astype('float16')        #todo remove fix if useless
    # Melt traffic data so each line contains data of one pollutant only

    # traffic_melt = pd.melt(traffic_overlay,
    #                        id_vars=['y', 'x'],
    #                        value_vars=traffic_pols,
    #                        var_name='pollutant_name') #todo: fix memory error'''

    #error "fix": break melt into chunks. it is slow. it is temporary.
    pivot_list = list()
    chunksize = 5000
    chunks = ([chunksize] * (traffic_overlay.shape[0] // chunksize))
    chunks.append(len(traffic_overlay) % chunksize)
    chunks.insert(0, 0)
    chunks = np.cumsum(chunks)
    for i in range(0, len(chunks) - 1):
        row_pivot = traffic_overlay.iloc[chunks[i]:chunks[i + 1]].melt(id_vars=['y', 'x'], value_vars=traffic_pols,
                                                                       var_name='pollutant_name')
        pivot_list.append(row_pivot)
    traffic_melt = pd.concat(pivot_list)    #todo figure our why MemoryError occurss here

    traffic_melt.set_index(['y', 'x', 'pollutant_name'], inplace=True)
    # Add the traffic data to the measurements
    stations['value'] = stations[['value']].add(
            traffic_melt.loc[stations.index.values])
    stations.dropna(inplace=True)
    if stations.shape[0] == 0:
        return None

    stations.rename(columns={'value': 'calc_val'}, inplace=True)
    stations.reset_index(drop=False, inplace=True)
    stations.drop(['x', 'y'], axis=1, inplace=True)
    return stations
