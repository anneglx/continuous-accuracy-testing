import pandas as pd
import numpy as np


def _remove_partial_results(results):
    mask = ~np.bitwise_or(np.isnan(results['true_val'].values),
                          np.isnan(results['calc_val'].values))
    if ~mask.any():
        return None
    return results.loc[mask].reset_index(drop=True)


def melt_result(res, data_removed):
    res_pivot = pd.melt(res,
                        id_vars=['lat', 'lon', 'lat_lon', 'algo_name'],
                        var_name='pollutant_name', value_name='calc_val')
    data_removed_pivot = pd.melt(data_removed.reset_index(inplace=False),
                                 id_vars=['lat', 'lon', 'lat_lon'],
                                 var_name='pollutant_name',
                                 value_name='true_val')
    res_pivot = pd.merge(res_pivot,
                         data_removed_pivot[['true_val', 'pollutant_name',
                                             'lat_lon']],
                         how='inner',
                         on=['pollutant_name', 'lat_lon'])
    res_pivot.reset_index(drop=True, inplace=True)
    return _remove_partial_results(res_pivot)


def format_result(result, cols2add=None):
    if type(result) is list:
        result = pd.concat(result, axis=0, ignore_index=True)
    if cols2add is not None:
        for col, values in cols2add.items():
            result = _add_col(result.copy(), col, values)
    return result


def _add_col(data, col, values):
    if col == 'units':
        if type(values) == str:
            data[col] = values
        else:
            data[col] = 'NaN'
            for pol, unit in values.items():
                data.loc[data['pollutant_name'] == pol, 'units'] = unit
    elif col == 'country_code':
        data[col] = 'PLACEHOLDER'
        for row in values.itertuples():
            data.loc[data.lat_lon == row.Index, 'country_code'] = row.country_code
    else:
        data[col] = values
    return data
