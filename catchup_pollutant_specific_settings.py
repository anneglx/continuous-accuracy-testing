'''This file has been copied as-is from the catchup repository'''

# Pollutants that should not have ML models
POLLUTANTS_WO_ML = ['trs', 'no', 'nmhc']

# Thresholds used to filter unreasonable predictions
# NOTE: we use these values in the CAT too:
#       continuous-accuracy-testing/cat/catchup_utils.py
PREDICT_THRESHOLDS = {
        'co': 100000,
        'no2': 2000,
        'o3': 1000,
        'pm10': 5000,
        'pm25': 2000,
        'so2': 1000
        }


# Priority for prediction methods
class Priority:
    """
    Priority for prediction methods

    The priority is determined by integer values: higher value = higher priority,
    with 0 as the default (lowest).

    Note
    ----
    Current implementation sets 'PurpleAir' with high priority for the whole
    world, for PM2.5. Please add additional rules to set other priorities
    inside specific bboxes.

    Attributes
    ----------
    PRIORITY_OPTIONS: list of dicts
        Holds the full priority rules for prediction.
        Each list item should have the key ``bbox`` and additional keys of
        pollutant names (at least one) for each relevant pollutant.
        The bbox list order is [sw_lat, sw_lon, ne_lat, ne_lon].
    relevant_priority : dict
        Will hold the priority rule for the current pollutant-bbox.

    Parameters
    ----------
    pollutant: str
        Name of the pollutant we are predicting for.
    coords: list of length 2
        Coordinates of the station we are predicting for, in the form [lat, lon].

    Usage notes
    -----------
    * bboxes should not overlap. If they do, stations found in the shared area
      will simply be assigned to the first match found (according to the order
      of PRIORITY_OPTIONS.
    * In case an item on PRIORITY_OPTIONS does not have any pollutant keys,
      it's equivalent to applying the default priority (0) to all pollutants
      and methods.
    * To apply a global rule, the last item on PRIORITY_OPTIONS should always
      be defined with a global bbox, e.g. [-90, -180, 90, 180].

    Example
    -------
    How to add priority limited to a bbox::

        PRIORITY_OPTIONS = [
            # Area 1
            {'bbox': [30, 40, 31, 41],
             'pm25': {'PurpleAir': 2, 'persistence': 1},
             'pm10': {'persistence': 1}},
            # Area 2
            {'bbox': [75, -120, 76, -119],
             'pm25': {'PurpleAir': 2}},
            # Rest of the world
            {'bbox': [-90, -180, 90, 180]}  # no special priority
            ]

    """
    PRIORITY_OPTIONS = [{'bbox': [-90, -180, 90, 180],
                         'pm25': {'PurpleAir': 1}}]

    relevant_priority = {}

    def __init__(self, pollutant, coords):
        for po in self.PRIORITY_OPTIONS:
            if self.point_in_bbox(coords, po['bbox']):
                self.relevant_priority = po.get(pollutant, {})
                break

    def point_in_bbox(self, coords, bbox):
        return (bbox[0] <= coords[0] <= bbox[2]) and (bbox[1] <= coords[1] <= bbox[3])
