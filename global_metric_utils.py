import pandas as pd
'''
from brz_baqi_calculator import BAQICalculator


def baqi_from_concentrations(concentrations, baqi_config):
    """
    Calculate per-measurement BAQI index and category from concentrations

    Uses BAQICalculator from api-libs brz_baqi_calculator, thus requiring an
    instance of the BAQI config as input.
    """
    baqi_calc = BAQICalculator()

    # Drop all non-BAQI pollutants
    BAQI_POLLUTANTS = list(baqi_config['pollutants']['pollutants'].keys())
    concentrations = concentrations[concentrations.pollutant_name.isin(BAQI_POLLUTANTS)]

    # Calculate pollutant BAQI values
    pollutant_baqi = concentrations[['calc_val', 'true_val']].copy()
    for value_field in ['calc_val', 'true_val']:
        pollutant_baqi[value_field] = concentrations.apply(
            lambda x: baqi_calc.baqi_for_pollutant(baqi_config, x['pollutant_name'], x[value_field]), axis=1)
    pollutant_baqi.rename(columns={'calc_val': 'calc_aqi', 'true_val': 'true_aqi'}, inplace=True)

    # Calculate pollutant BAQI categories (levels)
    pollutant_category = pollutant_baqi.copy()
    for value_field in ['calc_aqi', 'true_aqi']:
        pollutant_category[value_field] = pollutant_baqi.apply(
            lambda x: baqi_calc.category_from_aqi(baqi_config, x[value_field]), axis=1)
    pollutant_category.rename(columns={'calc_aqi': 'calc_category', 'true_aqi': 'true_category'}, inplace=True)

    # Merge pollutant BAQI, pollutant category and metadata dataframes
    baqi = pd.concat((concentrations.drop(['calc_val', 'true_val', 'units'], axis=1), pollutant_baqi, pollutant_category), axis=1)

    return baqi


def dominant_from_baqi(baqi):
    """
    Calculate dominant pollutant data from BAQI dataframe (e.g. output of
    `baqi_from_concentrations`).
    """
    # Pivot to station orientation
    pvt = baqi.pivot_table(
        index='lat_lon',
        columns='pollutant_name',
        values=['calc_aqi', 'true_aqi', 'calc_category', 'true_category'],
    )

    # Dominant pollutant (idxmin returns e.g. ('true_val', 'so2'), hence the applymap)
    dominant_pollutant = pvt[['calc_aqi', 'true_aqi']].groupby(axis=1, level=0).idxmin(axis=1).applymap(lambda x: x[1])
    dominant_pollutant.rename(columns={'calc_aqi': 'calc_pollutant', 'true_aqi': 'true_pollutant'}, inplace=True)
    # BAQI of dominant pollutant (minimum)
    dominant_aqi = pvt[['calc_aqi', 'true_aqi']].groupby(axis=1, level=0).min()
    # Category of dominant pollutant (maximum, since 0 = best, 4 = worst)
    dominant_category = pvt[['calc_category', 'true_category']].groupby(axis=1, level=0).max()
    # Metadata (lat, lon, country_code, etc.)
    dominant_meta = baqi.pivot_table(index='lat_lon', aggfunc='first').drop(['calc_aqi', 'calc_category', 'true_aqi', 'true_category', 'pollutant_name'], axis=1)

    # Merged dominant dataframes
    dominant = pd.concat((dominant_meta, dominant_pollutant, dominant_aqi, dominant_category), axis=1).reset_index()

    return dominant
'''