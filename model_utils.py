import numpy as np
import pandas as pd

from stations_utils import add_col_latlon
from results_utils import melt_result


def arrange_model_input(model):
    a = [dict(lat=x['lat'], lon=x['lon'], **x['pollutants']) for x in model]
    return pd.DataFrame(a).sort_values(['lat', 'lon']).reset_index(drop=True)


def _mask_model_by_bounds(model, bounds=[-90, 90, -180, 180]):
    """Clip the model's bounds to make sure the interpolation works"""
    model_ = model.copy()
    mask_lat = np.bitwise_and(model_['lat'] > bounds[0],
                              model_['lat'] < bounds[1])
    mask_lon = np.bitwise_and(model_['lon'] > bounds[2],
                              model_['lon'] < bounds[3])
    mask = np.bitwise_and(mask_lat, mask_lon)
    return model[mask]


def _intrpmodel(model, pollutants, Interpolator):
    """Create model interpolators"""
    model_ = model.copy()
    model_ = _mask_model_by_bounds(model)
    intrp = Interpolator(model_, pollutants)
    return intrp


def _calc_spline_interp(model, pollutants, Interpolator, lat, lon):
    """Calculate the RSBS Spline interpolation"""
    # Make the interpolator
    modelintrp = _intrpmodel(model, pollutants, Interpolator)

    d = {'lat': lat, 'lon': lon}
    for pol in pollutants:
        r = []
        for lat_, lon_ in zip(lat, lon):
            # Use the interpolator
            r.append(modelintrp.interpolators[pol](lat_, lon_))
        r = np.array(r).squeeze()
        d.update({pol: r})
    return d


def _calc_pwise_interp(model, pollutants, Interpolator, lat, lon):
    """Calculate the per-axis-piecewise bicubic splines interpolation"""
    d = {'lat': lat, 'lon': lon}
    for pol in pollutants:
        r = []
        for lat_, lon_ in zip(lat, lon):
            r.append(Interpolator(pd.DataFrame({'lat': [lat_], 'lon': [lon_]}),
                                  model, pol).loc[0, pol])
        d.update({pol: r})
    return d


def _calc_linear_interp(model, pollutants, Interpolator, lat, lon):
    """Calculate a simple linear interpolation"""
    d = {'lat': lat, 'lon': lon}
    for pol in pollutants:
        model_pol = model.pivot(index='lat', columns='lon', values=pol)
        r = Interpolator(
                points=(model_pol.index.values, model_pol.columns.values),
                values=model_pol.values,
                xi=np.stack((lat, lon), axis=1),
                method='linear',
                bounds_error=False,
                fill_value=np.nan)

        d.update({pol: r})
    return d


def _calc_nearest_grid(model, pollutants, Interpolator, lat, lon):
    """Calculate the Nearest Neighbor value"""
    d = {'lat': lat, 'lon': lon}
    KDTree = Interpolator
    # build a KDTree
    tree = KDTree(list(zip(model.lat.values, model.lon.values)))
    for pol in pollutants:
        # Create a pivoted table for each pollutant
        model_pol = model[['lat', 'lon', pol]].copy()
        model_pol = model_pol.pivot(index='lat', columns='lon')[pol]

        # Create the tree lat lon input array
        r = np.array([lat, lon]).T
        # Query the tree for the index in the source table
        dist, i = tree.query(r)
        # Retrieve the values for the model nearest grid point
        r = model.loc[i, pol].tolist()
        d.update({pol: r})
    return d


def get_model_at_points(model, stations, pollutants, interpolators, melt=True):
    """Interpolate the model at stations' locations"""
    stations.sort_values(['lat', 'lon'], inplace=True)
    lat = stations['lat'].values
    lon = stations['lon'].values

    res_all = []
    for Interpolator in interpolators:
        interp_name = Interpolator.__name__.lower()
        # Linear
        if interp_name in ['interpn', 'linear']:
            res = _calc_linear_interp(model, pollutants, Interpolator, lat,
                                      lon)
        # RSBS Spline
        elif interp_name in ['splineinterpolator', 'spline', 'rsbs']:
            res = _calc_spline_interp(model, pollutants, Interpolator, lat,
                                      lon)
        # Piece-wise Linear (slow)
        elif interp_name in ['pwise_pol', 'pwise', 'piece_wise']:
            res = _calc_pwise_interp(model, pollutants, Interpolator, lat, lon)

        # Retrieve the value at the closet model grid point
        elif interp_name in ['nearest_grid', 'kdtree']:
            res = _calc_nearest_grid(model, pollutants, Interpolator, lat, lon)

        else:
            raise Exception('Unknown interpolator name')

        res.update({'algo_name': [interp_name] * lat.shape[0]})
        res_all.append(pd.DataFrame(res))

    res_all = pd.concat(res_all, axis=0, ignore_index=True)
    res_all = add_col_latlon(res_all)
    if melt:
        return melt_result(res_all, stations)
    else:
        return (res_all, stations)


def get_model_bounds(model_data):
    """
    Return a list with the model's bounds. The order is lat_min, lat_max,
    lon_min, lon_max.
    """
    bounds = [model_data['lat'].min(),
              model_data['lat'].max(),
              model_data['lon'].min(),
              model_data['lon'].max()]
    return bounds


def _divide_cams_domain(locations, deg_interval):
    lat_range = [locations.lat.min(), locations.lat.max()]
    lon_range = [locations.lon.min() + 180, locations.lon.max() + 180]
    if lat_range[1] - lat_range[0] > deg_interval:
        lat_n_parts = int(
                np.ceil((lat_range[1] - lat_range[0]) / float(deg_interval))
                )
    else:
        lat_n_parts = 1
    if lon_range[1] - lon_range[0] > deg_interval:
        lon_n_parts = int(
                np.ceil((lon_range[1] - lon_range[0]) / float(deg_interval))
                )
    else:
        lon_n_parts = 1

    if lat_n_parts == 1 and lon_n_parts == 1:
        return [{'sw': (lat_range[0], lon_range[0] - 180),
                 'ne': (lat_range[1], lon_range[1] - 180)}]

    lat_values = np.linspace(lat_range[0], lat_range[1], num=lat_n_parts + 1)
    lon_values = np.linspace(lon_range[0], lon_range[1], num=lon_n_parts + 1)

    parts = []
    for latp in range(lat_n_parts):
        for lonp in range(lon_n_parts):
            parts.append({'sw': (lat_values[latp], lon_values[lonp] - 180),
                          'ne': (lat_values[latp + 1], lon_values[lonp + 1] - 180)})  # noqa: E501
    return parts


def _get_part_data(part, locations, cams_regional, cams_global):
    loc_part = _mask_by_coords(locations, part)
    camsr_part = _mask_by_coords(cams_regional, part, margin=3 * 0.1)
    camsg_part = _mask_by_coords(cams_global, part, margin=3 * 0.4)
    return loc_part, camsr_part, camsg_part


def _mask_by_coords(data, bounds, margin=0.0):
    mask_lat = np.bitwise_and(data.lat >= bounds['sw'][0] - margin,
                              data.lat <= bounds['ne'][0] + margin)
    mask_lon = np.bitwise_and(data.lon >= bounds['sw'][1] - margin,
                              data.lon <= bounds['ne'][1] + margin)
    masked = data[np.bitwise_and(mask_lat, mask_lon)].copy()
    return masked.sort_values(['lat', 'lon']).reset_index(drop=True)


def smoosh_interp_cams_w_algoman(
        algoman, locations, cams_regional, cams_global, deg_interval=10):
    """
    Use the algo-manager to interpolate the CAMS data and smoosh the
    two CAMS types together. Since the interpolation can be very heavy
    to calculate, it happens in parts (sized by ``deg_interval``).
    """
    domain_parts = _divide_cams_domain(
            cams_global[['lat', 'lon']], deg_interval)
    cams_tt_parts = []
    for part in domain_parts:
        loc_part, camsr_part, camsg_part = _get_part_data(
                part, locations, cams_regional, cams_global)
        # In case this is a part without stations - continue
        if loc_part.shape[0] == 0:
            continue
        # Make a regular grid from the gt stations locations
        lon, lat = np.meshgrid(loc_part.lon.drop_duplicates(),
                               loc_part.lat.drop_duplicates())
        grid = pd.DataFrame(
                {'lat': lat.flatten(), 'lon': lon.flatten()}
                ).sort_values(['lat', 'lon']).reset_index(drop=True)
        # Interpolate the CAMS data
        manager = algoman(
                grid, cams_regional=camsr_part, cams_global=camsg_part,
                cams_intrp_method='pwise')
        cams_tt_part = manager()
        # Choose only the relevant points from the regular grid
        cams_tt_part = pd.merge(
                loc_part, cams_tt_part, how='left', on=['lat', 'lon'])

        cams_tt_parts.append(cams_tt_part.copy())

    cams_tt = pd.concat(cams_tt_parts, axis=0, ignore_index=True)
    return cams_tt
