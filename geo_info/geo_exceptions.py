

class GeoBaseExceptions(Exception):
    """Base exception for all geo_info library
    
    """
    pass


class GeoIllegalIndexes(GeoBaseExceptions):
    """Represents the error where indexes are illegal"""
    pass

class GeoCoordinatesNotInTile(GeoBaseExceptions):
    """Represents the error when coordinates are not in the tile"""
