from __future__ import division
from math import ceil
from .geo_exceptions import GeoIllegalIndexes


class GeoPartition(object):
    """This class can partition a world/tile into smaller iterable parts
    
    This is useful for example when you need to part up the world,
    to smaller parts for digestion in algorithms
    
    Notes:
        Don't use this directly, use GeoInfo.split_to_parts or
        GeoInfo.split_to_size
        Also, this implementation is used as the underlying mechanism
        for Tiles, although Tiles are also partitionable themselves
    
    """
    def __init__(self, max_indexes, part_size):
        """Initialize a partitioner of the world/tile
         
        This will partition a set of grid points to equal size parts
        While equality might not be promised at the edges
        
        Args:
            max_indexes: tuple of ints
                either 2 ints or 4 ints representing the grid world
                to be partitioned, if 2 ints are passed they are the max
                and the min is assumed to be 0, this is passed in the format
                of (lat_index, lon_index).
                If 4 ints are passed, they are assumed to be a grid bounding
                box to partition and are given in the form of
                (west_lon_index, south_lat_index, 
                 east_lon_index, north_lat_index)
            part_size: int/tuple of ints
                either an int or a tuple of 2 ints representing the
                number of grid points in each part on either or both axes
                1 int is representing the number of grid points 
                in each part on both axes, and is for cases where 
                partitioning on both axes in equal measures
                2 ints is representing the number of grid points in
                each axis on his own in the form of (lat, lon), this
                is used for when partitioning in a different way on each axis
        Raises:
            GeoIllegalIndexes: 
                in cases where indexes passed do not make
                sense or follow the rules described above
        Notes:
            1. The multiple ways of initializing this class are kept for 
            backward compatibility, and should hopefully some day disappear
            or be replaced by a saner approach of doing it.
            2. A lot of the properties are lazy evaluated, as they are
            usually not needed
        """

        super(GeoPartition, self).__init__()

        # this function can except two types of input, all the indexes or
        # just the max indexes
        # But due to a weird standard that we follow, it goes like that
        # when we except the 4 (all indexes) we receive (lat, lon, lat, lon)
        # but if we except only maximums (only 2) we receive (lat, lon)
        if len(max_indexes) == 4:
            self.left = max_indexes[0]
            self.bottom = max_indexes[1]
            self.right = max_indexes[2]
            self.top = max_indexes[3]
        elif len(max_indexes) == 2:
            self.bottom = 0
            self.left = 0
            self.top = max_indexes[0]
            self.right = max_indexes[1]
        else:
            raise GeoIllegalIndexes('wrong number of indexes '
                                    '(2 or 4 are expected)')
        if self.bottom >= self.top or self.left >= self.right:
            raise GeoIllegalIndexes('The order of things is'
                                    '(low_lon, low_lat, high_lon, high_lat)'
                                    'and you got it wrong')

        # we can except either a single size for both lat and long
        # or a different size for each
        try:
            self.part_size_lat, self.part_size_lon = part_size
        except TypeError:
            self.part_size_lat = self.part_size_lon = part_size

        # this variables may not be needed, so they are lazy evaluated
        # (once if they are needed)
        self._max_part_id = None
        self._num_lat_parts = None
        self._num_lon_parts = None

    def get_lat_lon_num_parts(self):
        """Return the number of parts, given the initial values
        
        Returns: tuple int representing the number of parts lat, lon

        """
        if not self._num_lat_parts and not self._num_lon_parts:
            self._num_lat_parts = int(
                ceil((self.top - self.bottom) / self.part_size_lat)
            )
            self._num_lon_parts = int(
                ceil((self.right - self.left) / self.part_size_lon)
            )
        return self._num_lat_parts, self._num_lon_parts

    num_lat_lon_parts = property(get_lat_lon_num_parts)

    def get_max_part_id(self):
        """Returns the maximum part id
        
        Returns: int representing the maximum id

        """
        if not self._max_part_id:
            lat_parts, lon_parts = self.num_lat_lon_parts
            self._max_part_id = lat_parts * lon_parts
        return self._max_part_id

    max_part_id = property(
        get_max_part_id,
        doc='int maximum part id for this partitioning'
    )

    def get_grid_indexes(self):
        """Returns a tuple(int) of the bounding box of the world"""
        return self.left, self.bottom, self.right, self.top

    grid_indexes = property(
        get_grid_indexes,
        doc='tuple(int) bounding box of the world'
    )

    def get_part_indexes_by_id(self, part_id):
        """Convert a part id to the part indexes returns tuple(int) lat, lon"""
        max_lat_part_index, max_lon_part_index = self.num_lat_lon_parts
        lat_part_index = part_id // max_lon_part_index
        lon_part_index = part_id % max_lon_part_index
        return lat_part_index, lon_part_index

    def get_part_id_from_part_indexes(self, lat_part_index, lon_part_index):
        """Returns int part id from int part indexes"""
        max_lat_part_index, max_lon_part_index = self.num_lat_lon_parts
        part_id = lat_part_index * max_lon_part_index + lon_part_index
        return part_id

    def get_grid_indexes_from_part_indexes(self, lat_part_index,
                                           lon_part_index):
        """Returns the grid indexes of the part given by part indexes
        
        Args:
            lat_part_index: int the part index latitude part
            lon_part_index: int the part index longitude part

        Returns:
            tuple of ints representing the bounding box of the part
            as grid indexes

        """
        lat_low_grid_index = lat_part_index * self.part_size_lat + self.bottom
        lon_low_grid_index = lon_part_index * self.part_size_lon + self.left
        lat_high_grid_index = (
            (lat_part_index + 1) * self.part_size_lat + self.bottom
        )
        lon_high_grid_index = (
            (lon_part_index + 1) * self.part_size_lon + self.left
        )
        return (
            lon_low_grid_index,
            lat_low_grid_index,
            min(self.right, lon_high_grid_index),
            min(self.top, lat_high_grid_index),
        )

    def get_grid_indexes_from_part_id(self, part_id):
        """Returns the grid indexes of the part given by part id

        Args:
            part_id: int the part id (lower the part_id max)

        Returns:
            tuple of ints representing the bounding box of the part
            as grid indexes

        """
        return self.get_grid_indexes_from_part_indexes(
            *self.get_part_indexes_by_id(part_id)
        )

    def get_part_indexes_from_grid_indexes(self, grid_indexes):
        """Returns the part indexes where the lower left part of the grid is
        
        Args:
            grid_indexes: tuple ints representing grid indexes bounding box 

        Returns:
            tuple int representing the part indexes of the lower left corner
            of the grid indexes bounding box

        """
        left, bottom, top, right = grid_indexes

        return (
            (bottom - self.bottom)//self.part_size_lat,
            (left - self.left)//self.part_size_lon
        )

    def get_part_id_from_grid_indexes(self, indexes):
        """Returns the part id where the lower left part of the grid bbox is

        Args:
            indexes: tuple ints representing grid indexes bounding box 

        Returns:
            tuple int representing the part indexes of the lower left corner
            of the grid indexes bounding box

        """
        part_indexes = self.get_part_indexes_from_grid_indexes(indexes)
        return self.get_part_id_from_part_indexes(*part_indexes)










