""" A  set of tools useful for geo calculations

This module provides a set of functions that are useful in geographic calculations
 
geo_epsilon - A constant describing when 2 geo units are equal
EARTH_R - the Earth radius in km (Assuming a sphere)
"""
import math
from math import sin, cos, atan2, sqrt, radians, degrees


geo_epsilon = 0.000001
EARTH_R = 6372.8
EARTH_CIRCUMFERENCE = 2 * math.pi * EARTH_R


def calc_distance(lat1, lon1, lat2, lon2):
    """ Calculate the great circle distance between 2 points in km
    
    Args:
        lat1: a float describing the lat in degrees of point 1
        lon1: a float describing the lon in degrees of point 1
        lat2: a float describing the lat in degrees of point 2
        lon2: a float describing the lon in degrees of point 2

    Returns:
        a float, the distance in km

    """
    distance = _calc_distance(lat1, lon1, lat2, lon2)
    return distance


def calc_distance_at_lat(lat, lon1, lon2):
    """ Calculate the distance between 2 points sharing the same latitude
    
    The distance is the distance as if walking across the latitude line,
    and not the great circle distance
    
    Args:
        lat: The latitude of both points
        lon1: The longitude of point 1 as a float
        lon2: The longitude of point 2 as a float

    Returns:
        float the distance in km

    """
    dlon = lon2 - lon1
    distance = calc_distance(lat, lon1, lat, lon2)
    if dlon > 180:
        circumference_at_lat = EARTH_R * cos(radians(lat)) * 2 * math.pi
        return circumference_at_lat - distance
    return distance


def calc_middle_point(lat1, lon1, lat2, lon2):
    """ Calculate the location in degrees of the middle between 2 points
    
    This is done across the great circle, so the middle point across the 
    great circle
    
    Args:
        lat1: a float describing the lat in degrees of point 1
        lon1: a float describing the lon in degrees of point 1
        lat2: a float describing the lat in degrees of point 2
        lon2: a float describing the lon in degrees of point 2

    Returns:
        float, float the lat, lon in degrees of said point

    """
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    BX = cos(lat2) * cos(dlon)
    BY = cos(lat2) * sin(dlon)
    lat = atan2(sin(lat1) + sin(lat2), sqrt((cos(lat1) + BX)**2 + BY**2))
    lon = lon1 + atan2(BY, cos(lat1) + BX)
    return degrees(lat), degrees(lon)


def _calc_distance(lat1, lon1, lat2, lon2):

    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon1 - lon2

    cosl2 = cos(lat2)
    cosdl = cos(dlon)
    cosl1 = cos(lat1)
    sinl1 = sin(lat1)
    sinl2 = sin(lat2)

    y = sqrt(
        (cosl2 * sin(dlon)) ** 2
        + (cosl1 * sinl2 - sinl1 * cosl2 * cosdl) ** 2
        )
    x = sinl1 * sinl2 + cosl1 * cosl2 * cosdl
    c = atan2(y, x)
    return EARTH_R * c


def calc_angle_deg(lat1, long1, lat2, long2):
    return math.degrees(math.atan2(lat1 - lat2, long1 - long2))


def is_same_place(lat1, long1, lat2, long2):
    return (abs(lat1 - lat2) < geo_epsilon) and (abs(long1 - long2) < geo_epsilon)


def is_same_geo_unit(unit1, unit2):
    return abs(unit1 - unit2) < geo_epsilon
