""" This package is used for Geographic grid and partitioning

This is used throughout the system to split the world into tiles, and parts
And so that we can iterate through it

Example:
>>> from geo_info import GeoInfo
>>> gi = GeoInfo((-180, -90, 180, 90), 
...              grid_distance_meters=500, tile_size_lat=50, tile_size_lon=50)
>>> tile = gi.get_tile_containing_geo(35.777259, 139.668467) # somewhere in Tokyo
>>> tile.tile_indexes
(559, 1422)
"""
from .geo_info import GeoInfo
from .geo_iterator import GeoIterator
from .geo_exceptions import GeoBaseExceptions
from .geo_exceptions import GeoIllegalIndexes, GeoCoordinatesNotInTile
from .geo import calc_distance
from .geo import calc_angle_deg
from .geo import is_same_place
from .geo import calc_middle_point
from .geo import geo_epsilon, EARTH_R

# GeoInfo = GeoInfo
# GeoIterator = GeoIterator
# GeoIllegalIndexes = GeoIllegalIndexes
# calc_distance = calc_distance
# calc_angle_deg = calc_angle_deg
# is_same_place = is_same_place
