from __future__ import division
from .geo import calc_distance, calc_distance_at_lat
from .geo_iterator import GeoIterator
from .geo_partitioning import GeoPartition
from .geo_exceptions import GeoCoordinatesNotInTile


class GeoInfo(object):
    """Main class to do geo<->grid interpolation and traversal
    
    This the class controls the transfer between the geographical world,
    and the grid world that is used in our calculations.
    
    it allows you to tile, as well as part a world, and is variable about
    bounding boxes, size of tiles, and grid distance
    
    example:
        >>> from geo_info import GeoInfo
        >>> gi = GeoInfo((-180, -90, 180, 90), 
        ...              tile_size_lat=50, tile_size_lon=50)
        >>> gi.geo_to_index(35.644, 139.618)
        (27949, 71099)
        >>> gi.geo_to_index(gi.index_to_geo(27949, 71099))
        >>> tile = gi.get_tile_by_tile_indexes(559, 1422)
        >>> [round(x, 2) for x in tile.tile_geo_bbox]
        [139.62, 35.64, 139.84, 35.87]
        >>> tile.is_tile
        True
        >>> gi.is_tile
        False
        >>> tile.tile_grid_indexes
        (71100, 27950, 71150, 28000) 
    """
    def __init__(self, bounding_box, grid_distance_meters=500,
                 tile_size_lat=None, tile_size_lon=None, iterator=GeoIterator,
                 **kwargs):
        """Initialize the grid to the world that interest you
        
        This effect all parameters of the grid, the distance between 
        points will be grid_distance_meters in the longest part of 
        the bounding box
        
        
        Args:
            bounding_box: (west, south, east, north) the part of the 
                world of interest as floats  
            grid_distance_meters: An int describing the distance 
                between 2 grid points at the longest part
            tile_size_lat: Int size of each tile constituting the 
                bounding box in grid points 
            tile_size_lon: Int size of each tile constituting the 
                bounding box in grid points
            iterator: Iteration class for iterating over parts 
                when it is split
            **kwargs: Additional named arguments used mainly for 
                internal class uses (tile splitting)
        """
        # sw_lon, sw_lat, ne_lon, ne_lat = bounding_box
        self.iterator = iterator
        self.west, self.south, self.east, self.north = bounding_box
        assert self.north > self.south and self.east > self.west
        assert grid_distance_meters > 0
        self.grid_jump_meters = grid_distance_meters
        self.grid_to_km_multiplier = 1000 / self.grid_jump_meters

        # deltas are used as the key by which we traverse the grid
        # they set the equal length jump we use between points
        deltas = self._calc_deltas(
            (self.west, self.south, self.east, self.north)
        )

        self.delta_lat = deltas['delta_lat']
        # we need 2 delta longitudes, one for the grid
        # and another for creating effective boxes
        # some of the algorithms rely on a quick ability to
        # filter everything beyond a certain distance,
        # since the world is spherical (for our purpose)
        # the longitude changes between the different latitudes
        # we want the shorter one between them
        # for the quick effective distance filter
        self.delta_lon = deltas['delta_lon_long']
        self.delta_lon_effective_distance = deltas['delta_lon_short']

        # we set the indexes for the tiles
        # in case we do not have tile sizes (no tiling in this case
        # we simply exist within 1 tile
        max_lat, max_lon = self._get_max_global_indexes()
        self.tile_size_lat = tile_size_lat if tile_size_lat else max_lat
        self.tile_size_lon = tile_size_lon if tile_size_lon else max_lon
        self._is_tile = False
        self.part = GeoPartition(
            (max_lat, max_lon),
            (self.tile_size_lat, self.tile_size_lon)
        )
        self.tile_indexes = kwargs.get('tile_indexes', (0, 0))
        if 'tile_indexes' in kwargs:
            self._is_tile = True
            # since we are in a tile we want the shortest delta
            # for effective distance calculations
            deltas = self._calc_deltas(self.tile_geo_bbox)
            self.delta_lon_effective_distance = deltas['delta_lon_short']

    def _calc_delta(self, point1, point2, distance):
        """Return the degrees between 2 grid points
        
        Args:
            point1: Degrees location of western/southern part 
                (only lat or lon) 
            point2: Degrees location of western/southern part 
                (only lat or lon) should be lat or lon as the above
            distance: The distance between those 2 degrees that we want
                the grid degree for

        Returns:
            A float representing the degree difference 

        """
        return (
            abs(point1 - point2)/(distance * self.grid_to_km_multiplier)
        )

    def _calc_deltas(self, bbox):
        """Returns a dict containing all the interesting degree delta
        
        This function calculates and find the degree delta for the
        longest, shortest lon, and the lat for the given grid
        
        Args:
            bbox: tuple of floats describing the world for which we need this
                data

        Returns:
            a dictionary containing 
                delta_lat: the degree delta at the lat axis
                delta_lon_short: the longitude short distance
                delta_lon_long: the longitude at the longest part
            each one of them is a float, of the degrees between each
            grid point (delta_lat, delta_lon_long), or what it would
            have been has it been done in the shortest part.
            (The shortest part is good for effective bounding boxes)
        """
        west, south, east, north = bbox
        north_distance = calc_distance_at_lat(north, west, east)
        south_distance = calc_distance_at_lat(south, west, east)
        lon_distance_long = max(north_distance, south_distance)
        lon_distance_short = min(north_distance, south_distance)
        if north > 0 > south:
            # handle the equator because this is the longest in the bbox
            lon_distance_long = calc_distance_at_lat(0, west, east)
        lat_distance = calc_distance(south, west, north, west)
        return {
            'delta_lat': self._calc_delta(north, south, lat_distance),
            'delta_lon_short': self._calc_delta(
                east, west, lon_distance_short
            ),
            'delta_lon_long': self._calc_delta(east, west, lon_distance_long)
        }

    is_tile = property(
        lambda self: self._is_tile,
        doc='Answers the question is it a tile or a complete geo_info'
    )

    def get_tile_grid_indexes(self):
        """Returns the Tile grid indexes, the global ones if not a tile
        Returns:
            a tuple of 4 int grid indexes, in the form of
            west, south, east, north
        """
        if self.is_tile:
            return self.part.get_grid_indexes_from_part_indexes(
                *self.tile_indexes
            )
        lat_index, lon_index = self._get_max_global_indexes()
        return 0, 0, lon_index, lat_index

    tile_grid_indexes = property(
        get_tile_grid_indexes,
        doc='A 4 ints tuple of the indexes of this grid, or the globe'
            'if not a tile'
    )

    def geo_to_index(self, lat, lon):
        """Translate from world coordinates, to bottom left grid indexes
        
        Note:
            that since the grid is not as fine grained as the world
            coordinates, a point is selected that is for the purpose of this
            implementation the lower left corner for the grid points
            surrounding the point.
        
        Args:
            lat: float world decimal degrees 
            lon: float world decimal degrees

        Returns:
            tuple of 2 int grid indexes
        """
        lat_index = int((lat - self.south) / self.delta_lat)
        lon_index = int((lon - self.west) / self.delta_lon)
        return lat_index, lon_index

    def geo_to_closest_index(self, lat, lon):
        """Translate from world coordinates, to closest grid indexes """
        lat_index = int(round((lat - self.south) / self.delta_lat))
        lon_index = int(round((lon - self.west) / self.delta_lon))
        return lat_index, lon_index

    def geo_to_indexes_bbox(self, lat, lon):
        """This will return the grid indexes surrounding a coordinate
        
        Args:
            lat: float world decimal degrees 
            lon: float world decimal degrees

        Returns:
            tuple of 4 int grid indexes
        """
        sw_lat_index = int((lat - self.south) / self.delta_lat)
        sw_lon_index = int((lon - self.west) / self.delta_lon)
        ne_lat_index = sw_lat_index + 1
        ne_lon_index = sw_lon_index + 1
        return sw_lon_index, sw_lat_index, ne_lon_index, ne_lat_index

    def index_to_geo(self, lat_index, lon_index):
        """Translate from grid indexes to world coordinates
        
        Args:
            lat_index: int grid index
            lon_index: int grid index

        Returns:
            tuple of 2 float decimal degrees
        """
        lat = self.south + lat_index * self.delta_lat
        lon = self.west + lon_index * self.delta_lon
        return lat, lon

    def _get_max_global_indexes(self):
        return self.geo_to_index(self.north, self.east)

    def __iter__(self):
        return self.iterator(self)

    def split_to_size(self, size_lat, size_lon=None):
        """Partition a world/tile to smaller parts for digestion
        
        This splits a world/tile to smaller size parts containing size 
        parameters amount of grid points 
        (or less, in case of the ends, since it might not 
        divide completely) 
        
        if a size_lon is not given the world/tile will be split to
        lines size_lat size, and world lon size
        
        Args:
            size_lat: int amount of grid indexes 
            size_lon: int amount of grid indexes (optional) 

        Returns:
            list of iterators of type self.iterator 
            (default GeoIterator), on which you can iterate to 
            go through all the parts.
        """
        tile_grid_indexes = self.tile_grid_indexes
        if size_lon is None:
            size_lon = tile_grid_indexes[2] - tile_grid_indexes[0]
        partitioner = GeoPartition(tile_grid_indexes, (size_lat, size_lon))
        parts_iterators = list()
        for i in range(partitioner.max_part_id):
            left, bottom, right, top = (
                partitioner.get_grid_indexes_from_part_id(i)
            )
            iterator = self.iterator(
                geo_info=self, start_lat_index=bottom, start_lon_index=left,
                end_lat_index=top, end_lon_index=right
            )
            parts_iterators.append(iterator)
        return parts_iterators

    def split_to_parts(self, num_lat_parts, num_lon_parts=1):
        """Partition a world/tile to smaller parts for digestion

        This splits a world/tile to smaller size parts, of the amount
        set in num parameters of equal grid point amount
        (or less, in case of the ends, since it might not 
        divide completely) 

        if a num_lon_parts is not given the world/tile will be split to
        lines num_lat_parts size, and world lon amount
        
        Note:
            It will not create an n+1 parts, in any case

        Args:
            num_lat_parts: int amount of parts to create 
            num_lon_parts: int amount of parts to create (optional) 

        Returns:
            list of iterators of type self.iterator 
            (default GeoIterator), on which you can iterate to 
            go through all the parts.
        """
        left, bottom, right, top = self.tile_grid_indexes
        distance_lat, distance_lon = (right - left, top - bottom)
        # we do the next part, because if it is not equal then we need
        # to leave extra room
        if distance_lat % num_lat_parts == 0:
            num_lat_parts = num_lat_parts
        else:
            num_lat_parts = num_lat_parts - 1

        if distance_lon % num_lon_parts == 0:
            num_lon_parts = num_lon_parts
        else:
            num_lon_parts = num_lon_parts - 1

        size_lat = distance_lat//num_lat_parts
        size_lon = distance_lon//num_lon_parts
        return self.split_to_size(size_lat, size_lon)

    def get_effective_distance(self, distance_km):
        """Returns the effective distance in degrees for the world/tile
        
        Args:
            distance_km: float distance in km

        Returns:
            float the delta in decimal degrees
        """
        return (
            self.delta_lat * distance_km * self.grid_to_km_multiplier,
            self.delta_lon_effective_distance * distance_km *
            self.grid_to_km_multiplier
        )

    def get_effective_bounding_box(
            self, distance_km, effective_distances=None
    ):
        """Returns the decimal degrees of the ED bbox of self
        
        Given a distance this will return a (west, south , east, north),
        for a given world/tile containing the
        bounding box of this world/tile + effective distance,
        this is mainly used in calculation to get all the relevant stations
        for a tile, relevant cams points etc...
        
        Args:
            distance_km: float the distance in km for ED
            effective_distances: 
                list of lat and long containing the degrees
                delta that are needed for the effective distance
                bounding box, this overrides distance_km (optional)

        Returns:
            dict containing west, south, east, north decimal degrees
            of the ED bounding box
        """
        if not effective_distances:
            effective_distances = self.get_effective_distance(distance_km)
        west, south, east, north = self.tile_geo_bbox
        return {
            'south': south - effective_distances[0],
            'north': north + effective_distances[0],
            'west': west - effective_distances[1],
            'east': east + effective_distances[1],
        }

    def get_tile_max_id(self):
        """Returns the tile with the max ID of this world
        
        Returns:
            int tile id
        """
        return self.part.max_part_id

    tile_max_id = property(
        get_tile_max_id,
        doc='the max id of any tile in this world'
    )

    def get_num_lat_lon_tiles(self):
        """ Returns the number of tiles on each axis
        
        Returns:
            tuple containing lat, lon of the maximum tiles
        """
        return self.part.num_lat_lon_parts

    num_lat_lon_tiles = property(
        get_num_lat_lon_tiles,
        doc='tuple containing the maximum lat, lon of tiles'
    )

    def get_tile_by_tile_id(self, tile_id):
        """Returns a tile from a world, based on tile ID"""
        tile_indexes = self.part.get_part_indexes_by_id(tile_id)
        return self.get_tile_by_tile_indexes(*tile_indexes)

    def get_tile_by_tile_indexes(self, tile_lat_index, tile_lon_index):
        """Returns a tile for a world, based on tile indexes"""
        bbox = (self.west, self.south, self.east, self.north)
        return GeoInfo(
            bounding_box=bbox,
            grid_distance_meters=self.grid_jump_meters,
            tile_size_lat=self.tile_size_lat,
            tile_size_lon=self.tile_size_lon,
            iterator=self.iterator,
            tile_indexes=(tile_lat_index, tile_lon_index)
        )

    def get_tile_geo_bbox(self):
        """Returns the tile geographic bbox (4 corners of tile)
        
        Returns:
            tuple containing 4 float geo decimal coordinates
            in order (west, south, east, north)

        """
        tile_indexes = self.tile_grid_indexes
        south, west = self.index_to_geo(tile_indexes[1], tile_indexes[0])
        north, east = self.index_to_geo(tile_indexes[3], tile_indexes[2])
        return west, south, east, north

    tile_geo_bbox = property(
        get_tile_geo_bbox,
        doc='The tile geo bbox (4 corners of the tile) see: get_tile_geo_bbox'
    )

    def get_tile_containing_geo(self, lat, lon):
        """Returns the tile containing the closest grid point to the geo coord
        
        Args:
            lat: float geo decimal coordinate
            lon: float geo decimal coordinate

        Returns:
            Tile containing this geo point
        """
        grid_indexes = self.geo_to_closest_index(lat, lon)
        tile_indexes = (
            grid_indexes[0]//self.tile_size_lat,
            grid_indexes[1]//self.tile_size_lon
        )
        return self.get_tile_by_tile_indexes(*tile_indexes)

    def geo_to_tile_relative_indexes(self, lat, lon):
        """Returns the relative indexes of closest point inside the tile of geo
        
        Given a tile containing the geo decimal coordinates,
        returns a 0 based indexes of the grid index, inside the tile
        this is useful for file access or saved tile data information
        
        Args:
            lat: float geo decimal coordinate
            lon: float geo decimal coordinate

        Returns:
            tuple of 2 ints, representing the grid indexes, relative to
            this tile, of the lat, lon requested
        
        Raises:
            GeoCoordinatesNotInTile, in case that this is run on a world,
            or a tile that is not the tile containing those lat lon

        """
        grid_indexes = self.geo_to_closest_index(lat, lon)
        tile_grid_indexes = self.tile_grid_indexes
        lat_rel_grid_index = grid_indexes[0] - tile_grid_indexes[1]
        lon_rel_grid_index = grid_indexes[1] - tile_grid_indexes[0]
        lat_validity = 0 <= lat_rel_grid_index < self.tile_size_lat
        lon_validity = 0 <= lon_rel_grid_index < self.tile_size_lon
        if not (lat_validity and lon_validity):
            raise GeoCoordinatesNotInTile('Geo point not in tile')
        return lat_rel_grid_index, lon_rel_grid_index


