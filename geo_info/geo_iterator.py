from .geo_exceptions import GeoIllegalIndexes


class GeoIterator(object):
    """ This is the default iterator for GeoInfo object
    
    This will iterate over every grid point in the given tile/world
    """
    def __init__(self, geo_info,
                 start_lat_index=None, start_lon_index=None,
                 end_lat_index=None, end_lon_index=None):
        """Initialize the iterator with the needed information for grid
        
        this goes through all the lon at each lat, lat by lat
        
        Examples:
            >>> from geo_info import GeoInfo
            >>> gi = GeoInfo((-180, -90, 180, 90), 
            ...              tile_size_lat=50, tile_size_lon=50)
            >>> tile = gi.get_tile_by_tile_indexes(500, 500)
            >>> for i in tile: 
            ...     print(i)
        
        Args:
            geo_info: GeoInfo object representing the tile/world
            start_lat_index: int grid index where we start the lat iteration
            start_lon_index: int grid index where we start the lon iteration
            end_lat_index: int grid index where we end the lat iteration
            end_lon_index: int grid index where we end the lon iteration
        
        Raises:
            GeoIllegalIndexes - in cases where the end of the iteration
            is large then the max availalbe grid points
        
        Notes:
            This is mainly for use internally by GeoInfo
        """
        (
            min_lon_index, min_lat_index,
            max_lon_index, max_lat_index
        ) = geo_info.tile_grid_indexes

        if start_lat_index is not None:
            self.start_lat_index = start_lat_index
        else:
            self.start_lat_index = min_lat_index
        if end_lat_index is not None:
            self.end_lat_index = end_lat_index
        else:
            self.end_lat_index = max_lat_index
        if start_lon_index is not None:
            self.start_lon_index = start_lon_index
        else:
            self.start_lon_index = min_lon_index
        if end_lon_index is not None:
            self.end_lon_index = end_lon_index
        else:
            self.end_lon_index = max_lon_index
        self.id = '{start_lat}_{end_lat}_{start_lon}_{end_lon}'.format(
            start_lat=self.start_lat_index,
            end_lat=self.end_lat_index,
            start_lon=self.start_lon_index,
            end_lon=self.end_lon_index,
        )

        end_lat_invalid = self.end_lat_index > max_lat_index
        end_lon_invalid = self.end_lon_index > max_lon_index
        if end_lat_invalid and end_lon_invalid:
            raise GeoIllegalIndexes

        self.geo_info = geo_info
        self.generator_function = None

    def get_min_max_indexes(self):
        """Returns the start and end of this iteration"""
        return (
            self.start_lon_index,
            self.start_lat_index,
            self.end_lon_index,
            self.end_lat_index,
        )

    min_max_indexes = property(
        get_min_max_indexes,
        doc='Returns the start and end of this iteration'
    )

    def __iter__(self):
        if not self.generator_function:
            self.generator_function = self._next_generator()
        return self

    def __next__(self):
        if not self.generator_function:
            self.generator_function = self._next_generator()
        return next(self.generator_function)

    def next(self):
        return self.__next__()

    def _next_generator(self):
        for lat_index in range(self.start_lat_index, self.end_lat_index):
            for lon_index in range(self.start_lon_index, self.end_lon_index):
                lat, lon = self.geo_info.index_to_geo(lat_index, lon_index)
                yield {
                    'lat': lat, 'lon': lon,
                    'lat_index': lat_index, 'lon_index': lon_index
                }
