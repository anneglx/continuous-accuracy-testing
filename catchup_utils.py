import pandas as pd
import numpy as np
from stations_utils import add_col_latlon, _extract_units
from catchup_pollutant_specific_settings import Priority, PREDICT_THRESHOLDS
from copy import deepcopy


def arrange_prediction_input(raw_data):
    other_keys = set(raw_data[0].keys()) - set(['all_results'])
    a = []
    for line in raw_data:
        d = line['all_results']
        res_keys = list(d.keys())
        for k in other_keys:
            d[k] = [line[k]] * len(d[res_keys[0]])
        a.append(pd.DataFrame(d))
    prediction = pd.concat(a, ignore_index=True)
    # In case the concat changed the type of `value`
    if prediction['value'].dtype == 'O':
        prediction['value'] = prediction['value'].astype(float)
    return rename_prediction_columns(prediction)


def rename_prediction_columns(data):
    return data.rename(columns={'model_name': 'prediction_type',
                                'score': 'learn_score',
                                'value': 'calc_val',
                                'delay': 'orig_delay',
                                'station_id': 'lat_lon'})


def add_cams_to_prediction(prediction, cams_data):
    cams_melt = pd.melt(cams_data,
                        id_vars=['lat', 'lon', 'lat_lon'],
                        var_name='pollutant_name', value_name='calc_val')
    cams_melt['prediction_type'] = 'cams'
    cams_melt.set_index(['pollutant_name', 'lat_lon', 'prediction_type'],
                        inplace=True)
    prediction.set_index(['pollutant_name', 'lat_lon', 'prediction_type'],
                         inplace=True)
    prediction.update(cams_melt)
    return prediction.reset_index(drop=False)


def cleanup_cams_data(cams_data):
    cams_data.drop('algo_name', axis=1, inplace=True)
    cams_data = cams_data.groupby('lat_lon').first().reset_index(drop=False)
    return cams_data


def prepare_prediction_gt(raw_data):
    # Make DataFrame
    gt = pd.DataFrame(raw_data)
    # Remove non-hourly pollutants
    mask = ~gt['pollutant_name'].str.contains('hr')
    gt = gt[mask]
    # Extract units
    pol_unit = _extract_units(gt.copy())
    # Rearrange columns
    gt.drop(['validity', 'datetime', 'local_id', 'units'], axis=1, inplace=True)
    gt.rename(columns={'value': 'true_val'}, inplace=True)

    # Group
    gt = add_col_latlon(gt.copy())

    grouped = gt.groupby(['lat_lon', 'pollutant_name'])

    if 'country_code' in gt.columns:
        return pd.concat(
            [grouped.agg({'country_code': 'first'}), grouped.mean()], axis=1
            ).reset_index(drop=False), pol_unit
    else:
        return grouped.mean().reset_index(drop=False), pol_unit


def add_persistence_values(prediction_best, old_persistence):
    # Prepare persistence data
    pers_values = prepare_prediction_gt(old_persistence)[0]
    pers_values.rename(columns={'true_val': 'persistence_val'}, inplace=True)
    pers_values.drop(['lat', 'lon'], axis=1, inplace=True)
    pers_values.set_index(['pollutant_name', 'lat_lon'], inplace=True)
    # Combine
    prediction_best.set_index(['pollutant_name', 'lat_lon'], inplace=True)
    return prediction_best.join(pers_values).reset_index()


def get_best_predictions(prediction_full):
    prediction_best = []
    grouped = prediction_full.groupby(['lat_lon', 'pollutant_name'])
    for idx, group in grouped:
        group_best = mimic_catchup_prediction_result(group)
        if group_best is not None:
            prediction_best.append(group_best)
    return pd.concat(prediction_best, ignore_index=True)


def mimic_catchup_prediction_result(prediction):
    '''Choose the best method the same way it is done in the CatchUp'''

    # Convert into dict to match the way it works in the catchup
    if type(prediction) == pd.Series:
        prediction_dict = prediction.to_dict()
    elif type(prediction) == pd.DataFrame:
        prediction_dict = prediction.to_dict(orient='records')

    # Recreate the PredictionResult object, as it was in the catchup
    prediction_result = PredictionResult()
    for pred_single in prediction_dict:
        prediction_result.update_pollutant(pred_single['pollutant_name'])
        prediction_result.update_priority(pred_single['pollutant_name'],
                                          [pred_single['lat'], pred_single['lon']])
        prediction_result.append_(
                    value=pred_single['calc_val'],
                    score=pred_single['learn_score'],
                    model_name=pred_single['prediction_type'],
                    delay=pred_single['orig_delay']
                    )

    # CatchUp "patch" - Remove ML results for some pollutants/countries
    prediction_result = filter_ml(prediction_result,
                                  pred_single['lat'], pred_single['lon'],
                                  pred_single['pollutant_name'],
                                  pred_single['country_code'])

    # Use the catchup's own method for choosing the best model
    best_result = prediction_result.get_best_result()

    if best_result is not None:
        # Add back the priority value
        best_idx = prediction_result.prediction['model_name'].index(best_result['model_name'])
        best_result['priority'] = prediction_result.prediction['priority'][best_idx]

        # Rename keys to match dataframe format
        name_changes = [('score', 'learn_score'),
                        ('value', 'calc_val'),
                        ('model_name', 'prediction_type')]
        for key, new_key in name_changes:
            best_result[new_key] = best_result.pop(key)

        # Add all other missing keys
        missing_keys = set(prediction.columns.tolist()).difference(set(best_result.keys()))
        _ = [best_result.update({key: pred_single[key]}) for key in missing_keys]

        # Convert to dataframe
        best_result = pd.DataFrame(best_result, index=[0])

    return best_result


def filter_ml(prediction_result, lat, lon, pollutant, country):
    '''CatchUp "patch" - Remove ML results for some pollutants/countries'''

    disable_ml_pd = {
        'pm25': 'any country',
        'pm10': 'any country',
        'o3': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'no2': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'so2': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'co': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'no': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'nox': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'c6h6': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'nh3': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'trs': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf'],
        'nmhc': ['mex', 'ind', 'chl', 'chn', 'esp', 'ita', 'jpn', 'tha', 'zaf']
        }

    pred_res_copy = deepcopy(prediction_result)

    # Remove [ML PD] as potential best method
    if (disable_ml_pd[pollutant] == 'any country') or (country in disable_ml_pd[pollutant]):
        if 'ml_pd' in pred_res_copy.prediction['model_name']:
            index = pred_res_copy.prediction['model_name'].index('ml_pd')
            for k in pred_res_copy.prediction.keys():
                pred_res_copy.prediction[k].pop(index)

    # Remove [ML NPD] as potential best method
    if 'ml_npd' in pred_res_copy.prediction['model_name']:
        index = pred_res_copy.prediction['model_name'].index('ml_npd')
        for k in pred_res_copy.prediction.keys():
            pred_res_copy.prediction[k].pop(index)

    return pred_res_copy


class PredictionResult():
    '''
    [Copied as-is from the catchup repository]

    Holds the CatchUp prediction results.

    Attributes
    ----------
    prediction : (dictionary)
        The full result. Each item in the dict is a list. All lists are of
        equal length, which is the number of models used.
    best_result : (dictionary or None)
        Initialized as None. After calling ``get_best_result()`` will change to
        a dict. Each dict item is a single value (instead of a list as in
        ``prediction``).

    Note
    ----
    ``self.prediction['priority']`` holds the priority of the models, not
    taking the accuracy into account.
    For example, when ``PRIORITY_METHODS`` is set to::
        PRIORITY_METHODS = {'pollutantX': {'methodA': 2, 'methodB': 1},
                            'pollutantY': {'methodB': 1}}
    * For pollutantX, methodA will always have the highest priority (if
      available), second is methodB (if available). After that come all the other
      methods, with priority = 0. Later they are ordered by their accuracy score,
      while maintaining the methods in ``PRIORITY_METHODS`` as the first ones.
    * For pollutantY, methodB has the highest priority and will be used if
      available. All other methods will be ordered by their accuracy.

    Methods
    -------
    get_full_result :
        Returns ``prediction``
    get_best_result :
        Sorts the results by their scores and returns the best one that is
        also valid (e.g. not None or negative).
    '''

    def __init__(self):
        self.prediction = {'score': [],
                           'value': [],
                           'model_name': [],
                           'delay': [],
                           'priority': []}
        self.best_result = None
        self.pollutant = None

    def append_(self, value=None, score=None, model_name=None, delay=None):
        if score is None:
            # Change any None score into an integer, to enable sorting
            score = -999
        self.prediction['value'].append(value)
        self.prediction['score'].append(score)
        self.prediction['model_name'].append(model_name)
        self.prediction['delay'].append(delay)
        self.prediction['priority'].append(self.priority.get(model_name, 0))

    def update_pollutant(self, pollutant):
        self.pollutant = pollutant

    def update_priority(self, pollutant, coords):
        self.priority = Priority(pollutant, coords).relevant_priority

    def get_full_result(self):
        return self.prediction

    def get_best_result(self):
        # TODO: deal with Nones?

        # NOTE: if we change the sort to reverse=False, the priority will not work
        scores = self.prediction['score']
        priority = self.prediction['priority']
        indexes = list(range(len(scores)))
        indexes_sorted = [x for _, _, x in sorted(zip(priority, scores, indexes), reverse=True)]  # noqa; E501

        # Go over the results, from priority, to best, to worst
        # Return the first valid result
        for index in indexes_sorted:
            value = self.prediction['value'][index]
            model_name = self.prediction['model_name'][index]
            if self.reasonable_value(value, model_name):
                self.best_result = {
                            'score': self.prediction['score'][index],
                            'value': self.prediction['value'][index],
                            'model_name': self.prediction['model_name'][index]}
                return self.best_result

        # If we did not find any viable prediction
        return None

    def reasonable_value(self, value, model_name):
        if value is None:
            return value

        # Use static thresholds to filter unreasonable predictions
        if model_name in ['ml_pd', 'ml_npd']:
            threshold = PREDICT_THRESHOLDS.get(self.pollutant, float('inf'))
        else:
            threshold = float('inf')

        return 0 <= value <= threshold
