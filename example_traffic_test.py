'''
testing!
temporary - to alter before real use
python<=3.7
'''

from cat import AccuracyTest
import pandas as pd
import time
import json


filename_to = ['test/2021-01-13T14_00_00_traffic_short.json', 'test/traffic_pollution_grid_2021-06-26T14_00_00.json']
filename_ai = ['test/2021-01-13T14_00_00_algo_input_measurements_short.json', 'test/2021-06-26T14_00_00_algo_input_measurements.json']
filename_m = ['test/2021-01-13T14_00_00_measurements_short.json', 'test/2021-06-26T14_00_00_measurements.json']

for i in range(0, 2):
    # traffic_overlay_at_stations
    with open(filename_to[i], 'r') as f:
        traffic_overlay = json.load(f)['traffic_pollution_grid']
    print('traffic', type(traffic_overlay), len(traffic_overlay))
    with open(filename_m[i], 'r') as f:
        ground_truth = json.load(f)['measurements']
    #print(ground_truth[:100])      #todo delete
    with open(filename_ai[i], 'r') as f:
        a = json.load(f)['measurements']
        algo_input = pd.DataFrame(a)
    if i ==1:
        algo_input.drop(columns=['data_time'], inplace=True)
        algo_input['validity'] = 'true'
        algo_input.rename(columns={'pollutant': 'pollutant_name', 'data_time': 'datetime'}, inplace=True)
        #print('algo:', algo_input.columns)     #todo delete
        algo_input = algo_input.to_dict('records')

    stime = time.time()
    res = AccuracyTest(
            to_test=traffic_overlay, interp_algo=None, ground_truth=ground_truth,
            extra_input={'algo_input_measurements': algo_input}).traffic_overlay_at_stations()

    res['absolute_error'] = abs(res['calc_val']-res['true_val'])
    print(str(i) + ': elapsed time ' + str(round(time.time() - stime, 2)))
    res.to_csv('testing-result_type' + str(i)+str(i) + '.csv')
