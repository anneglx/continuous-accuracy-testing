import pandas as pd
import numpy as np
import time
import json

from results_utils import format_result
from stations_utils import (arrange_stations_input,
                             arrange_traffic_input,
                             add_traffic_overlay,
                             groupby_latlon,
                             loo,
                             mask_stations_by_bounds,
                             add_col_latlon)

def std(x):
    return np.std(x)

class AccuracyTest(object):
    def __init__(self, to_test, interp_algo, ground_truth=None, cols2add=None,
                 extra_input=None):
        self.to_test = to_test
        self.ground_truth = ground_truth
        self.interp_algo = interp_algo
        self.extra_input = extra_input
        if cols2add is None:
            self.cols2add = dict()
        else:
            self.cols2add = cols2add

    def _verify_input(self, both=True, need_algo=True, extra_keys=None):
        if need_algo:
            if self.interp_algo is None:
                raise Exception('Missing interpolation algorithm(s).')

    def traffic_overlay_at_stations(self):
        self._verify_input(both=True, need_algo=False,
                           extra_keys='algo_input_measurements')

        # Rearrange true measurements
        gt = pd.DataFrame(self.ground_truth)
        country = gt[['lat', 'lon']].copy()
        gt = add_col_latlon(gt).groupby(['lat_lon', 'pollutant_name']).agg(
            {'lat': 'first', 'lon': 'first',
             'units': 'first', 'value': 'mean'})
        # Rearrange algo input measurements
        tt = pd.DataFrame(self.extra_input['algo_input_measurements'])
        tt['was_fixed'] = ~tt['value_before_traffic_fixes'].isnull()
        #tt.drop('value_before_traffic_fixes', axis=1, inplace=True)
        tt = add_col_latlon(tt).groupby(['lat_lon', 'pollutant_name']).agg(
            {'lat': 'first', 'lon': 'first',
             'value': 'mean', 'was_fixed': 'max'})
        tt.reset_index(drop=False, inplace=True)
        # Combine with the traffic overlay
        tt = add_traffic_overlay(tt, self.to_test.copy())
        # Combine gt and tt
        gt_cols = list(set(gt.columns.tolist()) - {'lat', 'lon'})
        res = pd.merge(
            tt.set_index(['lat_lon', 'pollutant_name']), gt[gt_cols],
            left_index=True, right_index=True).reset_index(drop=False)
        res.rename(columns={'value': 'true_val'}, inplace=True)

        # Add country code column
        res = pd.merge(res, country, on=['lat', 'lon'])

        # Add columns to result
        self.cols2add.update({'test_name': 'traffic_overlay_at_stations'})

        return format_result(res, self.cols2add)



def main():
    print(pd.__version__)
    # allocate memory for the result
    # res = np.zeros((3, 11168232), dtype='uint64')

    filename_to = 'test\\traffic_pollution_grid_2021-06-26T14_00_00.json'
    filename_ai = 'test\\algo_input_measurements_2021-06-26T14_00_00.json'
    filename_m = 'test\\measurements_2021-06-26T14_00_00.json'

    with open(filename_to, 'r') as f:
        a = json.load(f)['traffic_pollution_grid']
    traffic_overlay = a

    #print('traffic overlay: ', traffic_overlay.columns, traffic_overlay.shape)

    with open(filename_m, 'r') as f:
        a = json.load(f)['measurements']
    ground_truth = a

    #print('measurements: ', ground_truth.columns, ground_truth.shape)

    with open(filename_ai, 'r') as f:
        a = json.load(f)['measurements']
    algo_input = pd.DataFrame(a).rename(columns={'pollutant': 'pollutant_name'}, axis=1, inplace=True)
    a = algo_input.to_json()
    algo_input = json.load(a)

    '''
    print(algo_input.columns, algo_input.shape)

    print('before traffic fixes: ', set(algo_input['value_before_traffic_fixes']))
    print(set(algo_input[['pollutant_name', 'value']]))
    print('algo input source: ', set(algo_input['source']))

    algo_input = algo_input[:1000]
    ground_truth = ground_truth[:1000]
    traffic_overlay = traffic_overlay[:1000]'''

    res = AccuracyTest(
        to_test=traffic_overlay, interp_algo=None, ground_truth=ground_truth,
        extra_input={'algo_input_measurements': algo_input}
    ).traffic_overlay_at_stations()
    res.to_csv('result1.csv')
    res['absolute_error'] = abs(res['calc_val'] - res['true_val'])




if __name__=="__main__":
    main()

"""
The system will consist of a python library, wrapped and run on servers in the Cloud. The results will be exported to a table in BigQuery.
The tests’ results will be sortable by: station ID, pollutant, data timestamp, test timestamp, country, scraper, test type, interpolation method (where relevant).




"""