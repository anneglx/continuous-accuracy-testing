'''
upload test cat
'''
import pandas as pd
import numpy as np

from model_utils import (arrange_model_input,
                          get_model_bounds,
                          get_model_at_points,
                          smoosh_interp_cams_w_algoman)
from stations_utils import (arrange_stations_input,
                             arrange_traffic_input,
                             add_traffic_overlay,
                             groupby_latlon,
                             loo,
                             mask_stations_by_bounds,
                             add_col_latlon)
from catchup_utils import (arrange_prediction_input,
                            prepare_prediction_gt,
                            add_cams_to_prediction,
                            cleanup_cams_data,
                            get_best_predictions,
                            add_persistence_values)
#from .global_metric_utils import baqi_from_concentrations, dominant_from_baqi
from results_utils import format_result


class AccuracyTest(object):
    """
    Create an object with all the data and methods needed to run an accuracy
    test on our data. Bear in mind that each test-type requires a different
    kind of input.

    Parameters
    ----------
    to_test : list of dictionaries
        The data to test. Used to calculate the interpolation. Keys should
        include coordinates ('lat', 'lon'), and pollutants' names (e.g. 'o3').
    ground_truth : list of dictionaries
        The expected result. In these locations we calculate the interpolation.
        Keys should include coordinates ('lat', 'lon'), and pollutants' names
        (e.g. 'o3').
    interp_algo : list of functions
        The spatial interpolation functions to run.
    cols2add : dictionary
        Columns to add to the final result. Each dict key will be a column
        name; each dict value will be duplicated to fill the column. Important
        note: for `traffic_vs_all` and `traffic_vs_ambient`, `cols2add` *must*
        include a `ctr_for_traffic` list of dicts (measurements) with country
        code data.
    extra_input : dictionary
        Keys should correspond to the test's expected input. Values are the
        extra data required by the test. For example:
        ``{'cams': list, 'old_persistence': list}``
    """

    def __init__(self, to_test, interp_algo, ground_truth=None, cols2add=None,
                 extra_input=None):
        self.to_test = to_test
        self.ground_truth = ground_truth
        self.interp_algo = interp_algo
        self.extra_input = extra_input

        if cols2add is None:
            self.cols2add = dict()
        else:
            self.cols2add = cols2add

    def global_accuracy_metric(self):
        """
        Join results of Global Accuracy Metric test, performed in production
        GCE algorithm instances, with measurements ground-truth.

        Returns
        -------
        Pandas DataFrame with columns:
        ``calc_val, country_code, lat, lat_lon, lon, pollutant_name, test_name,
        true_val, units`` + columns from ``cols2add``
        """

        # Read input
        tt = pd.DataFrame(self.to_test.copy())
        gt = pd.DataFrame(self.ground_truth.copy())

        # Drop irrelevant columns
        tt.drop(['relevance_time'], axis=1, inplace=True)
        gt.drop(['datetime', 'local_id', 'validity'], axis=1, inplace=True)

        # Drop multi-hour pollutants and pollen
        drop_pols = ['co_8hr', 'general_kafun', 'grass', 'pm10_24hr', 'pm25_24hr', 'trees']
        tt = tt[~tt.pollutant_name.isin(drop_pols)]
        gt = gt[~gt.pollutant_name.isin(drop_pols)]

        # Add lat_lon column, set lat_lon and pollutant as index.
        # The `gropuby` -> `agg` flow allows properly handling any possible
        # station-pollutant duplicities
        gt = add_col_latlon(gt).groupby(['lat_lon', 'pollutant_name']).agg(
            {'lat': 'first', 'lon': 'first', 'units': 'first', 'country_code': 'first', 'value': 'mean'})
        tt = add_col_latlon(tt).groupby(['lat_lon', 'pollutant_name']).agg(
            {'lat': 'first', 'lon': 'first', 'calc_val': 'mean'})

        # Merge (inner join) dataframes
        res_all = tt.join(gt, how='inner', lsuffix='_tt')
        res_all.drop(['lat_tt', 'lon_tt'], axis=1, inplace=True)
        res_all.rename(columns={'value': 'true_val'}, inplace=True)
        res_all.reset_index(inplace=True)

        # Add columns to result
        self.cols2add.update({'test_name': 'global_accuracy_metric'})

        if len(res_all) > 0:
            return format_result(res_all, self.cols2add)
        else:
            return None

    def global_accuracy_metric_baqi(self):
        """
        Calculate BAQI index, category and dominance for global accuracy metric
        data.

        Utilizes `AccuracyTest.global_accuracy_metric` for initial
        concentrations dataframe, from which BAQI is calculated.

        Returns
        -------
        Tuple containing two DataFrames.
            merged: Measurements-oriented (long-form) BAQI DataFrame with
                    columns ``country_code, lat, lat_lon, lon, pollutant_name,
                    relevance_time, test_name, test_time, calc_aqi, true_aqi,
                    calc_category, true_category``.
            dominant: Stations-oriented dominant BAQI dataframe with columns
                      ``lat_lon, country_code, lat, lon, relevance_time,
                      test_name, test_time, calc_pollutant, true_pollutant,
                      calc_aqi, true_aqi, calc_category, true_category``.
        """
        # Extract BAQI config
        baqi_config = self.cols2add.pop('baqi_config')
        if baqi_config is None:
            raise Exception('Expected BAQI config, received None')

        # Get concentrations from `AccuracyTest.global_accuracy_metric`
        concentrations = self.global_accuracy_metric()
        if concentrations is None:
            return None

        # Generate BAQI (index and category) results from concentrations
        baqi_results = baqi_from_concentrations(concentrations, baqi_config)

        # Generate dominant results from BAQI results
        dominant_results = dominant_from_baqi(baqi_results)

        return (baqi_results, dominant_results)

    def loo_interp(self):
        """
        Perform a leave-one-out (LOO) test to evaluate the accuracy of a
        spatial interpolation method, **without** the effects of delay and
        missing data. This test is performed with data relevant to a **specific
        hour**.

        Returns
        -------
        Pandas DataFrame with columns:
        ``test_name, lat, lon, lat_lon, pollutant_name, units, calc_val,
        true_val`` + columns from ``cols2add``
        """

        # Rearrange input
        self._verify_input(both=False)
        tt, pol_unit = arrange_stations_input(self.to_test.copy())

        # Unite measurements by rounded lat, lon
        tt = groupby_latlon(tt.copy())

        # Do the LOO test
        tt_without_ctr = tt.drop('country_code', axis=1)
        res_all = loo(tt_without_ctr, tt_without_ctr.copy(), self.interp_algo)

        # Add columns to result
        self.cols2add.update({'units': pol_unit})
        self.cols2add.update({'test_name': 'loo_interp'})
        self.cols2add.update({'country_code': tt[['country_code']]})

        if len(res_all) > 0:
            return format_result(res_all, self.cols2add)
        else:
            return None

    def loo_run(self):
        """
        Perform a leave-one-out (LOO) test to evaluate the accuracy of a
        spatial interpolation method, **considering** the effects of delay and
        missing data. This test is performed with data relevant to a **specific
        algorithm run**.

        Returns
        -------
        Pandas DataFrame with columns:
        ``test_name, lat, lon, lat_lon, pollutant_name, units, calc_val,
        true_val`` + columns from ``cols2add``
        """

        # Rearrange input
        self._verify_input(both=True)
        tt, _ = arrange_stations_input(self.to_test.copy(), get_units=False)
        gt, pol_unit = arrange_stations_input(self.ground_truth.copy())

        # Unite measurements by rounded lat, lon
        tt = groupby_latlon(tt.copy())
        gt = groupby_latlon(gt.copy())

        gt_without_ctr = gt.drop('country_code', axis=1)

        # Do the LOO test
        res_all = loo(tt, gt_without_ctr, self.interp_algo)

        # Add columns to result
        self.cols2add.update({'units': pol_unit})
        self.cols2add.update({'test_name': 'loo_run'})
        self.cols2add.update({'country_code': gt[['country_code']]})

        if len(res_all) > 0:
            return format_result(res_all, self.cols2add)
        else:
            return None

    def loo_traffic_vs_ambient(self):
        """
        Perform a leave-one-out (LOO) test to evaluate the accuracy of the
        fixed traffic stations data when compared to **ambient stations** data.

        Returns
        -------
        Pandas DataFrame with columns:
        ``test_name, lat, lon, lat_lon, pollutant_name, units, calc_val,
        true_val`` + columns from ``cols2add``
        """

        # Rearrange input
        self._verify_input(both=False)
        fixed, ambient, pol_unit = arrange_traffic_input(self.to_test)
        if fixed is None or ambient is None:
            return None

        # Unite measurements by rounded lat, lon
        fixed = groupby_latlon(fixed.copy())
        ambient = groupby_latlon(ambient.copy())

        # Do the LOO test
        res_all = loo(fixed, ambient, self.interp_algo)

        ctr_for_traffic = self.cols2add.pop('ctr_for_traffic')
        ctr_codes = groupby_latlon(arrange_stations_input(ctr_for_traffic)[0])[['country_code']]

        # Add columns to result
        self.cols2add.update({'units': pol_unit})
        self.cols2add.update({'test_name': 'loo_traffic_vs_ambient'})

        if len(res_all) > 0:
            res = format_result(res_all, self.cols2add)
            res['country_code'] = 'PLACEHOLDER'
            for row in ctr_codes.itertuples():
                res.loc[res.lat_lon == row.Index, 'country_code'] = row.country_code
            return res
        else:
            return None

    def loo_traffic_vs_all(self):
        """
        Perform a leave-one-out (LOO) test to evaluate the accuracy of the
        fixed traffic stations data when compared to **all other algo-input**
        data (ambient + traffic).

        Returns
        -------
        Pandas DataFrame with columns:
        ``test_name, lat, lon, lat_lon, pollutant_name, units, calc_val,
        true_val`` + columns from ``cols2add``
        """

        # Rearrange input
        self._verify_input(both=False)
        fixed, ambient, pol_unit = arrange_traffic_input(self.to_test)
        if fixed is None or ambient is None:
            return None

        # Combine the data to form the ground truth
        gt = pd.concat([fixed, ambient], axis=0, ignore_index=True)

        # Unite measurements by rounded lat, lon
        fixed = groupby_latlon(fixed.copy())
        gt = groupby_latlon(gt.copy())

        # Do the LOO test
        res_all = loo(fixed, gt, self.interp_algo)

        ctr_for_traffic = self.cols2add.pop('ctr_for_traffic')
        ctr_codes = groupby_latlon(arrange_stations_input(ctr_for_traffic)[0])[['country_code']]

        # Add columns to result
        self.cols2add.update({'units': pol_unit})
        self.cols2add.update({'test_name': 'loo_traffic_vs_all'})

        if len(res_all) > 0:
            res = format_result(res_all, self.cols2add)
            res['country_code'] = 'PLACEHOLDER'
            for row in ctr_codes.itertuples():
                res.loc[res.lat_lon == row.Index, 'country_code'] = row.country_code
            return res
        else:
            return None

    def traffic_overlay_at_stations(self):
        """
        Test concentrations at a grid point where there are both traffic
        overlay data and a station: compare the traffic overlay +
        algo-input-measurement against the true-measurement.

        Requires ``extra_input``:
            ``{'algo_input_measurements': list of dictionaries}``
        """
        self._verify_input(both=True, need_algo=False,
                           extra_keys='algo_input_measurements')

        # Rearrange true measurements
        gt = pd.DataFrame(self.ground_truth)
        country = gt[['lat', 'lon', 'country_code']].copy()     #cc
        gt = add_col_latlon(gt).groupby(['lat_lon', 'pollutant_name']).agg(
            {'lat': 'first', 'lon': 'first',
             'units': 'first', 'value': 'mean'})
        print(gt.head(), len(gt), type(gt), 'tst', 1)    #todo:delete later
        # Rearrange algo input measurements
        tt = pd.DataFrame(self.extra_input['algo_input_measurements'])
        tt['was_fixed'] = ~tt['value_before_traffic_fixes'].isnull()
        tt.drop('value_before_traffic_fixes', axis=1, inplace=True)
        tt = add_col_latlon(tt).groupby(['lat_lon', 'pollutant_name']).agg(
            {'lat': 'first', 'lon': 'first',
             'value': 'mean', 'was_fixed': 'max'})
        tt.reset_index(drop=False, inplace=True)
        print(tt.head(), len(tt), type(tt), 'tst', 2)    #todo:delete later

        # Combine with the traffic overlay
        tt = add_traffic_overlay(tt, self.to_test.copy())
        print('traffic len', tt.shape, 'type', type(tt),  3)   #todo:delete later
        # Combine gt and tt
        gt_cols = list(set(gt.columns.tolist()) - {'lat', 'lon'})
        print(gt_cols, 3.5)
        res = pd.merge(
                tt.set_index(['lat_lon', 'pollutant_name']), gt[gt_cols],
                left_index=True, right_index=True).reset_index(drop=False)
        res.rename(columns={'value': 'true_val'}, inplace=True)
        print('result pre-country-merge len', len(res), 'test number: ', 4)  #todo:delete later
        # Add country code column
        res = pd.merge(res, country, on=['lat', 'lon'], how='inner')
        res.drop_duplicates()
        # Add columns to result
        self.cols2add.update({'test_name': 'traffic_overlay_at_stations'})
        print('result:', 'length', len(res), 'type', type(res), 'test number:', 5)  # todo:delete later
        return format_result(res, self.cols2add)

    def model_grid(self):
        """
        Compare an external model's data (``to_test``) to stations data
        (``ground_truth``).

        Optional ``extra_input`` - relevant only for HRRR data:
        ::
            {'smoke_pm10_factor': int or float}

        Returns
        -------
        Pandas DataFrame with columns:
        ``test_name, lat, lon, lat_lon, pollutant_name, units, calc_val,
        true_val`` + columns from ``cols2add``
        """
        # Rearrange model input
        self._verify_input(both=True)

        if isinstance(self.to_test, pd.DataFrame):
            tt = self.to_test.copy()
        else:
            tt = arrange_model_input(self.to_test.copy())

        # In case this is the HRRR model, multiply pm25 data to get pm10 data
        if 'pm10' not in tt.columns:
            if self.extra_input.get('smoke_pm10_factor', None):
                tt['pm10'] = tt['pm25'] * self.extra_input['smoke_pm10_factor']
            else:
                # TODO: raise exception? return None?
                return None

        # Rearrange stations input and unite measurements by rounded lat, lon
        gt, pol_unit = arrange_stations_input(self.ground_truth.copy())
        gt = groupby_latlon(gt.copy())

        # Filter stations by model bounds
        model_bounds = get_model_bounds(tt)
        gt_part = mask_stations_by_bounds(gt.copy(), model_bounds)
        if gt_part.shape[0] == 0:
            return None

        # Filter stations' pollutants by model data
        pollutants = sorted(list(set(tt.columns).difference(
                ('lat', 'lon'))))
        gt_part = gt_part[['lat', 'lon'] + pollutants]

        # Interpolate the model at stations locations
        res = get_model_at_points(tt, gt_part, pollutants, self.interp_algo)

        # Add columns to result
        self.cols2add.update({'units': pol_unit})
        self.cols2add.update({'test_name': 'model_grid'})
        self.cols2add.update({'country_code': gt[['country_code']]})

        return format_result(res, self.cols2add)

    def catchup_prediction(self):
        """
        Align predicted data and measurements data for comparison.
        Add missing CAMS data to the tested data (uses interpolation).
        """
        # Rearrange input
        self._verify_input(both=True, need_algo=True,
                           extra_keys=['cams_regional', 'cams_global'])
        tt = arrange_prediction_input(self.to_test)
        cams_r = arrange_model_input(self.extra_input['cams_regional'])
        cams_g = arrange_model_input(self.extra_input['cams_global'])
        gt, pol_unit = prepare_prediction_gt(self.ground_truth.copy())

        # Smoosh the two CAMS together and interpolate to the gt locations
        gt_locations = gt.groupby('lat_lon').mean()[['lat', 'lon']]
        gt_locations = gt_locations.sort_values(
                ['lat', 'lon']).reset_index(drop=False)

        cams_tt = smoosh_interp_cams_w_algoman(
                self.interp_algo[0], gt_locations, cams_r, cams_g)

        # Insert CAMS data into tt table
        tt = add_cams_to_prediction(tt, cams_tt)

        # Unite prediction and ground truth
        gt_cols = list(set(gt.columns.tolist()) - {'lat', 'lon'})
        res = pd.merge(tt, gt[gt_cols], on=['lat_lon', 'pollutant_name'])

        # Add columns to result
        self.cols2add.update({'units': pol_unit})
        self.cols2add.update({'test_name': 'catchup_prediction'})

        return format_result(res, self.cols2add)

    def catchup_best_method(self):
        """
        Find the best prediction method for every station-pollutant. These
        best-methods' results are the ones used in our production algorithm.

        Utilizes `AccuracyTest.catchup_prediction` for initial predictions
        dataframe.

        Returns
        -------
        Tuple containing two DataFrames.
            all:
                 Identical to the result of `AccuracyTest.catchup_prediction`.
                 Measurements-oriented (long-form) DataFrame with the default
                 columns ``lat, lat_lon, lon, pollutant_name, prediction_type,
                 test_name, calc_val, true_val, orig_delay, learn_score,
                 units``.
            best:
                  Stations-oriented dataframe with a single row for each
                  station-pollutant, with the default columns
                  ``lat, lat_lon, lon, pollutant_name, prediction_type,
                  test_name, calc_val, true_val, persistence_val, orig_delay,
                  learn_score, units``..
        """
        self._verify_input(
                both=True, need_algo=True,
                extra_keys=['cams_regional', 'cams_global', 'old_persistence'])
        # Get all catchup results
        predictions_all = self.catchup_prediction()
        # Find the best result for each station-pollutant
        predictions_best = get_best_predictions(predictions_all.copy())
        # Add the old persistence values for comparison
        predictions_best = add_persistence_values(
                predictions_best, self.extra_input['old_persistence'])
        # Change test name
        predictions_best['test_name'] = 'catchup_best_prediction'
        return (predictions_all, predictions_best)

    def stations_dynamic_distances(self):
        """
        Re-calculate stations radii, as they are calculated in the algorithm.
        The radii are the max and min distances used to combine stations and
        CAMS (or any other model). The radii can be dynamic if there is a smoke
        plume next to the station.

        Requires ``extra_input``:
        ::

            {'hrrr': list of dictionaries,
             'fmi': DataFrame,
             'mind': int or float,
             'maxd': int or float,
             'r2km': function such as algoman.conversion.r2km,
             'calibration_function': function such as algoman.smoke.calibrate_smoke}

        Requires ``self.interp_algo``:
        ::

            [algoman.smoke.calc_stations_dynamic_distances]

        Note
        ----
        This test now requires algoman version >= 3.0.2 and
        HRRR data *after preprocessing*.

        Returns
        -------
        Pandas DataFrame with columns:
        ``test_name, lat, lon, lat_lon, country_code, max_distance,
        min_distance, units`` + columns from ``cols2add``
        """
        self._verify_input(
            both=False, need_algo=True,
            extra_keys=['mind', 'maxd', 'r2km', 'calibration_function']
            )
        # Handle stations
        tt, pol_unit = arrange_stations_input(self.to_test.copy())
        tt = groupby_latlon(tt.copy())

        # Arrange and calibrate smoke model(s)' data
        hrrr = self.extra_input.get('hrrr', None)
        hrrr_available = hrrr is not None
        if hrrr_available:
            hrrr = arrange_model_input(hrrr)
            hrrr = self.extra_input['calibration_function'](hrrr, model_type='regional')

        fmi = self.extra_input.get('fmi', None)  # FMI data is already a DataFrame
        fmi_available = fmi is not None
        if fmi_available:
            fmi = self.extra_input['calibration_function'](fmi, model_type='global')

        # Calculate the dymanic stations' distances
        mind = self.extra_input['mind']
        maxd = self.extra_input['maxd']
        mind_vector, maxd_vector = self.interp_algo[0](hrrr, fmi, tt, mind, maxd)

        # Add the result to the stations data as new columns while
        # converting from radians to kilometers
        res = tt[['lat', 'lon', 'country_code']].copy().reset_index(drop=False)
        res['max_distance'] = self.extra_input['r2km'](maxd_vector)
        res['min_distance'] = self.extra_input['r2km'](mind_vector)

        # Add indication of which models were available
        if hrrr_available and fmi_available:
            model_name = 'both'
        elif hrrr_available:
            model_name = 'hrrr'
        elif fmi_available:
            model_name = 'fmi'
        else:
            model_name = 'none'

        # Define additional columns to be added to the result
        self.cols2add.update({'model_name': model_name})
        self.cols2add.update({'units': 'km'})
        self.cols2add.update({'test_name': 'stations_dynamic_distances'})

        return format_result(res, self.cols2add)

    def _verify_input(self, both=True, need_algo=True, extra_keys=None):
        if need_algo:
            if self.interp_algo is None:
                raise Exception('Missing interpolation algorithm(s).')

        if both:
            if self.to_test is None or self.ground_truth is None:
                raise Exception('Missing input data. This test requires both '
                                '`test` and `truth` input.')
        else:
            if self.to_test is None:
                if self.ground_truth is None:
                    raise Exception('Missing input data. This test requires '
                                    'either `test` or `truth` input.')
                else:
                    self.to_test = self.ground_truth.copy()

        if extra_keys is not None:
            if self.extra_input is None:
                raise Exception('Missing extra input data.')
            elif type(extra_keys) == list:
                for k in extra_keys:
                    if self.extra_input.get(k, None) is None:
                        raise Exception('Missing extra input data. '
                                        'key = "{}"'.format(k))
            elif type(extra_keys) == str:
                if self.extra_input.get(extra_keys, None) is None:
                    raise Exception('Missing extra input data. '
                                    'key = "{}"'.format(extra_keys))

    def _calc_statistics():
        #TODO:
        raise NotImplementedError
        return
